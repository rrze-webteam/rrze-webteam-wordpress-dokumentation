# RRZE Webteam WordPress Dokumentation

* [Aktuelle Plugins auf der CMS-Instanz](Plugins.md) - Liste der aktuellen Plugins und ihr Status
* [Fehlende Plugins](Fehlende-Plugins.md) - Liste der noch nicht existierenden Plugins, die in Zukunft bereitgestellt oder entwickelt werden sollten
* [Lizenz Plugins](Lizensierte-Plugins.md).- Liste der Fremd-Plugins für die eine Lizenz erworben wurde
* [Externe Plugins](Externe-Plugins.md) - Plugins, auch freie aus der WPD, die von anderen entwickelt werden, die aber keine Lizenzkosten verursachen


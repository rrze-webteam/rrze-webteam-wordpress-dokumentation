# Externe Plugins

Plugins, die von anderen Entwicklern bereitgestellt oder gepflegt werden oder solchen, für die wir einen eigenen Klon betreiben.

|Plugin-Name|Kurzbeschreibung|Links|Status|Zuständigkeit|
|---|---|---|---|---|
|fau-cris|  Ausgabe von Daten aus FAU CRIS| [GitHub](https://github.com/RRZE-Webteam/fau-cris) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/fau-cris/)|Extern|FAU CRIS Support 
|like-json|  k.A. | - | Prüfen ob noch gebraucht  | Webmaster LIKE
|Regenerate Thumbnails| Wird benötigt um Bildsizes neu zu berechnen, wenn diese von Themes nachträglich geändert wurden|[WordPress Directory](https://wordpress.org/plugins/regenerate-thumbnails/)|Extern|
|contact-form-7 (CF7)| Kontaktformular|[GitHub](https://github.com/RRZE-Webteam/contact-form-7) [Docs](https://www.wordpress.rrze.fau.de/plugins/inhaltsseiten-mit-funktionen-erweitern/contact-form-7/)|Aktuell| CF7 & RRZE
|cf7-conditional-fields| CF7-Erweiterung|[GitHub](https://github.com/RRZE-Webteam/cf7-conditional-fields) [Docs](https://conditional-fields-cf7.bdwm.be/conditional-fields-for-contact-form-7-tutorial/)|Extern - Aktuell| Jules Colle & RRZE


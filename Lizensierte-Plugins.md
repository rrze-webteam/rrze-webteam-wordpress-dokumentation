# Lizensierte Plugins

Plugins die von externen Firmen eingekauft, aber durch das RRZE betreut werden:

|Plugin-Name|Kurzbeschreibung|Links|Status|Nutzung|Zuständigkeit|
|---|---|---|---|---|---|
|the-events-calendar|Ausgabe von Terminen und Veranstaltungen|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/the-events-calendar) [GitLab](https://gitlab.rrze.fau.de/rrze-webteam/events-calendar-pro) [Docs](https://www.wordpress.rrze.fau.de/plugins/spezialplugins/the-events-calendar/)|Lizenz abgelaufen; Funktion duch RRZE Events ersetzt|-|[TEC](https://theeventscalendar.com) & RRZE
|the-events-calendar PRO |Ausgabe von Terminen und Veranstaltungen|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/the-events-calendar) [GitLab](https://gitlab.rrze.fau.de/rrze-webteam/events-calendar-pro) [Docs](https://www.wordpress.rrze.fau.de/plugins/spezialplugins/the-events-calendar/)|Lizenz abgelaufen; Funktion duch RRZE Events ersetzt|-|[TEC](https://theeventscalendar.com) & RRZE
|WS-Form PRO  |Kontaktformular mit benutzerfreundlichem UI|[Docs](https://www.wordpress.rrze.fau.de/plugins/inhaltsseiten-mit-funktionen-erweitern/ws-form/)|Aktuell|32 von 836 (3,8%) Sites.|WS Form
|WS-Form Pro - PDF  |Kontaktformular mit benutzerfreundlichem UI||Extern|24 von 836 (2,9%) Sites.|WS Form

# Fehlende Plugins - Welche Plugins sollten wir entwickeln

Für einige Aufgaben gibt es noch keine Plugins; Hier wäre es sinnvoll diese entweder auf eine ToDo zu stellen oder in nächster Zeit anzugehen.

|Möglicher Titel|Zweck (Kurzbeschreibung)|Verknüpfungen / Stakeholder|GitHub/Lab-Projekt|Priorität
|---|---|----|---|---|
| FAU Statistiken | Bereitstellung von FAU Statistiken wie bspw. Anzahl der Studierenden, Profs, etc. | DIP, Referat für Statistik | -- | gering
| StudOn Import | Import der Veranstaltungen und Lehrobjekte aus StudOn. Es soll auch möglich sein, aktuelle Kurse als Kursübersicht mit Anmeldelinks etc in eigene Websites einzubinden| StudOn | -- |mittel
| MOVE/DMS-Anbindung| Die ZUV führt seit einigen Jahren ein DMS im Rahmen des Projektes MOVE ein. Hierbei werden zentrale Dokumente in einem DMS abgelegt.  Dokumente, die darin abgelegt wurden, aber dann öffentlich gemacht werden soll, müssen derzeit extra in die FAUBox oder in Websites kopiert werden, was ungut ist. Eine Anbindung an das DMS wäre daher anzustreben. Ansprechpartner für das MOVE-projekt ist Christopher Schmidt. Ggf in Zusammenhang mit dem Remoter- und dem Downloads-Plugin. | MOVE-Projekt  | -- | gering
| ~~RRZE-Thesis~~ | Verwaltung (und Sync?) von Studien- und Abschlussarbeiten. Ersatz für die bisherige Funktion aus UnivIS. Zentrales Portal (à la Serviceportal) oder zwischen Websites abrufbar (à la FAQ) <br> Laut Info von Monica wurde inzwischen beschlossen, dies in StudIn zu machen. Daher erstmal on hold. Ob und welches Plugin dann von StudOn zur Verfügung gestellt wird, ist offen (WW, 2.5.2024)| UnivIS-Ablöse, CIO | -- | keine, da zurückgestellt. Wurde von ILI in StudOn angeboten, ohne jegliche Website-Einbindung.
| RRZE Visualisierungen | Ews gibt den Bedarf, dass Grafiken und Visualisierungen und auch Charts einheitlich und möhglichst barrierefrei dargestellt werden können. Auch sollen Highchars eingebunden werden können, wie auch komplexe Formeln etc. Dabei soll der Aufwand für die Autoren möglichst klein sein. Der AUtor soll, auch bei der Verwendung von Highcharts, kein JavaScript eingeben können müssen. Eine Umsetzung mit dem Block-Editor only wäre hier wahrscheinlich die beste Lösung| Highcharts, LaTeX | --- | mittel
| RRZE Cloud Embed| Einbinden von Daten aus verschiedenen Cloudsystemen, wie insbesondere FAUBox (primär zu entwickeln), aber auch andere gängige Cloudsysteme wie Nextcloud. |FAUBox| -- | mittel
| FAU Möbel- und Rechnerbörse| Ersatz für die wegfallende UnivIS Möbel-/Rechnerbörse|UnivIS-Ablöse, CIO | -- | gering
| FAU Studium / Studienfachanteile | Anzeige der Grafiken und/oder Tabellen der Studienfachanteile zu gegebenen Studiengängen| API.fau.de, Campo, MeinStudium| .. | hoch
| FAU Studium / Modulhandbücher | Neuentwicklung der Anzeige der Modulhandbücher für Studiengänge. Als Alternative zur Darstellung in Campo mit der Option diese in den Studiengangswebsites selbst anzuzeigen. Vorbild ist hier neben der Darstellung in Campo die ehemalige Darstellung in UnivIS (siehe vorherige Semester, z.B. SoSe 2018) | Campo, MeinSTudium, CIO | .. | mittel
| RRZE Block / Shortcode Usage Tracker | Erstellt eine neue Settingsseite (und ggf. auch ein Dashboard-Widget) auf der alle auf der Website verwendeten Shortcodes (sowohl inaktive als auch aktive) gelistet sind, mit Angabe und verlinkung der jeweiligen Seiten und anderer Post-Types. Ausserdem zusätzlich dasslebe auch für Blöcke. Ein Vorbild für Blöcke ist ggf. dieses Plugin: https://github.com/robertdevore/block-usage-tracker |||



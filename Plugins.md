# RRZE Plugins



## TOP 5 ToDo Plugins

Diese Plugins sollten als erstes und so schnell wie möglich mit dynamischen Block Editor Funktionen ausgestattet werden.
Vgl [Creating dynamisc blocks](https://developer.wordpress.org/block-editor/how-to-guides/block-tutorial/creating-dynamic-blocks/).  
Workarounds mit dem JavaScript, welches im Editor selbst die Optionen erfragten ohne die Panel-Funktionen und keine Preview zeigen, sind mittelfristig dynamische Block Editor Funktionen zu ersetzen.

Die folgende Liste zeigt die Top 5 Plugins, die mit höchster Priorität und als nächstes auf dynamische Blöcke umgestellt werden sollten.

|Plugin-Name|Kurzbeschreibung| Git |Wer ist dran?
|---|---|---|---|
|rrze-lectures  |Ausgabe der Campo Lehrveranstaltungen|[GitHub](https://github.com/RRZE-Webteam/rrze-lectures) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/lehrveranstaltungen/)|  NN
|MeinStudium|||
|CRIS|||NN
|rrze-calendar|||NN



## Weitere Plugins

* [Fehlende Plugins](Fehlende-Plugins.md): Welche Plugins sollten wir in naher oder mittlerer Zukunft entwickeln und bereitstellen
* [Lizensierte Plugins](Lizensierte-Plugins.md): Welche Plugins wurden über eine kostenpflichtige Lizenz betrieben
* [Externe Plugins](Externe-Plugins.md): Von anderen entwickelte und gepflegte Plugins


##  Plugins mit Aktualisierungsbedarf oder in aktueller Bearbeitung
  

|Plugin-Name|Kurzbeschreibung|Links|Wer ist dran?
|---|---|---|---|
| FAU-Studium | Darstellung der Studiengangsinformationen auf MeinStudium und via Plugins auf Websites | [https://github.com/RRZE-Webteam/fau-studium](https://github.com/RRZE-Webteam/fau-studium) |  Fa. Syde
| rrze-faudir | Darstellung des Personen- und Einrichtungsverzeichnis FAUdir auf Websites | [https://github.com/RRZE-Webteam/rrze-faudir](https://github.com/RRZE-Webteam/rrze-faudir) |  WW
|rrze-elements-blocks  | Gestaltungselemente inkl. Galerie, CTA und Co.|[GitHub](https://github.com/RRZE-Webteam/rrze-elements) [Docs](https://www.wordpress.rrze.fau.de/plugins/inhaltsseiten-mit-funktionen-erweitern/elements/)|Barbara / Lukas
|rrze-advanced-search| Erweiterte Suchmaschine für www.fau.de. Im Rahmen der Suchprojekts neu zu machen|[https://github.com/RRZE-Webteam/rrze-search](https://github.com/RRZE-Webteam/rrze-search) | 

## Aktuelle Plugins

|Plugin-Name|Kurzbeschreibung|Links|ToDos oder Info|Ansprechpartner
|---|---|---|---|---|
| RRZE-Designsystem |  Plugin zur Darstellung eines Design Systems in einer WordPress-Instanz . Eine erste Version sollte bis spätestens zum Herbst bereitgestellt werden. Für dieses Plugin sollten wir gleich auf "Block Editor Only" gehen können. Das Plugin könnte anhand des aktuellen Design Manuals erprobt werden, so daß wir nicht mit Inhalten auf die Deisgnagentur warten müssen. | [https://github.com/RRZE-Webteam/rrze-designsystem](https://github.com/RRZE-Webteam/rrze-designsystem) |  Nur Projekt- und Dokuseite | Lukas und Benjamin
|rrze-typesettings|Code-Highlighter|[GitHub](https://github.com/RRZE-Webteam/rrze-typesettings)||Benjamin
|rrze-faq| Anlegen und Ausgeben von FAQs|[GitHub](https://github.com/RRZE-Webteam/rrze-faq) [Docs](https://www.wordpress.rrze.fau.de/plugins/neue-inhaltstypen-erstellen/rrze-faq/)|Aktuell|Benjamin
|rrze-synonym | Pflegen und Ausgeben von Synonymen|[GitHub](https://github.com/RRZE-Webteam/rrze-synonym) [Docs](https://www.wordpress.rrze.fau.de/plugins/neue-inhaltstypen-erstellen/rrze-synonym/)| | Benjamin
|rrze-video |  Ausgabe von Videos|[GitHub](https://github.com/RRZE-Webteam/rrze-video) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/rrze-video/)| |Lukas
|rrze-newsletter |Erstellung und Versand von Newslettern in WordPress|[GitHub](https://github.com/RRZE-Webteam/rrze-newsletter) [Docs](https://www.wordpress.rrze.fau.de/plugins/spezialplugins/rrze-newsletter/)| | Rolf
|rrze-glossary |  Erstellung und Ausgabe eines Glossars|[GitHub](https://github.com/RRZE-Webteam/rrze-glossary) [Docs](https://www.wordpress.rrze.fau.de/plugins/neue-inhaltstypen-erstellen/rrze-glossary/)|Aktuell| Benjamin
|rrze-multilang   |Übersetzung von CMS-Seiten mit einer oder mehreren CMS-Instanzen|[GitHub](https://github.com/RRZE-Webteam/rrze-multilang) [Docs](https://www.wordpress.rrze.fau.de/plugins/inhaltsseiten-mit-funktionen-erweitern/rrze-multilang/)|Aktuell| Rolf
|rrze-jobs  |Ausgabe von FAU Jobs|[GitHub](https://github.com/RRZE-Webteam/rrze-jobs) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/jobs/)|| Barbara
|rrze-post-expiration|  Ablaufdatum für Beiträge|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-post-expiration) [Docs](https://www.wordpress.rrze.fau.de/plugins/inhaltsseiten-mit-funktionen-erweitern/rrze-post-expiration/)|Aktuell|  Rolf
|rrze-autoshare|Automatische Teilen von Titel, Auszugstext und Beitragsbildern von Beiträgen auf Bluesky, Mastodon und X (Twitter).|[GitHub](https://github.com/RRZE-Webteam/rrze-autoshare) [Docs](https://www.wordpress.rrze.fau.de/plugins/beitraege-teilen/rrze-autoshare/)|Aktuell|Rolf
|rrze-ac   |Zugriffskontrolle für einzelne Seiten und Medien|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-ac) [Docs](https://www.wordpress.rrze.fau.de/plugins/zugang-beschraenken/access-control/)|Aktuell|Rolf
| RRZE Events|  Eventverwaltungsfunktionen. Ergänzung mit weiteren hilfreichen Funtionen für eine Eventverwaltung für Kongresse und anderen Verwaltungen in denen es Vorträge, Vortragende und Termine für die Veranstaltungen gibt.  | [https://github.com/RRZE-Webteam/RRZE-Events](https://github.com/RRZE-Webteam/RRZE-Events)|Aktuell|Barbara
|rrze-legal   |Ausgabe der rechtlichen Pflichttexte und des Cookie Banners|[GitHub](https://github.com/RRZE-Webteam/rrze-legal) [Docs](https://www.wordpress.rrze.fau.de/plugins/neue-inhaltstypen-erstellen/rrze-legal/)|
|rrze-calendar   |Import und Ausgabe öffentlicher Veranstaltungen der FAU.|[GitHub](https://github.com/RRZE-Webteam/rrze-calendar) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/rrze-calendar/)|
|cms-workflow|Erleichtert den redaktionellen Workflow. Ermöglicht Rollenverwaltung und Versionierung. Beinhaltet auch alte Übersetzungsfunktion.|[GitHub](https://github.com/RRZE-Webteam/cms-workflow) [Docs](https://www.wordpress.rrze.fau.de/plugins/spezialplugins/cms-workflow/)|| Rolf
|rrze-cf7-redirect   |Erweiterung des Contact Form Plugins: Redirect nach Einreichung|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-cf7-redirect)|| Rolf
|rrze-cf7-save  |Erweiterung des Contact Form Plugins: Speichern von Einsendungen|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-cf7-save)||Rolf
|rrze-expo|   Erstellung und Ausgabe digitaler Messen|[GitHub](https://www.wordpress.rrze.fau.de/plugins/neue-inhaltstypen-erstellen/rrze-expo/) [Docs](https://www.wordpress.rrze.fau.de/plugins/neue-inhaltstypen-erstellen/rrze-expo/)|Aktuell|Barbara
|rrze-pwa   |Bereitstellung einer PWA der Website|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-pwa) [Docs](https://www.wordpress.rrze.fau.de/plugins/spezialplugins/progressive-web-app-pwa/)|Aktuell|Rolf
|rrze-statistik  |Ausgabe der Statistiken von statistiken.rrze.fau.de im Dashboard|[GitHub](https://github.com/RRZE-Webteam/rrze-statistik) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/rrze-statistik/)||Lukas 
|rrze-rss   |Ausgabe eines RSS-Feeds|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-rss) [Docs](https://www.wordpress.rrze.fau.de/plugins/beitraege-teilen/rrze-rss/)
|rrze-private-site  |Zugangskontrolle für die gesamte Website|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-private-site) [Docs](https://www.wordpress.rrze.fau.de/plugins/zugang-beschraenken/private-site/)|Aktuell|Rolf
|rrze-elements   |Gestaltungselemente inkl. Galerie, CTA und Co.|[GitHub](https://github.com/RRZE-Webteam/rrze-elements) [Docs](https://www.wordpress.rrze.fau.de/plugins/inhaltsseiten-mit-funktionen-erweitern/elements/)||Barbara / Lukas
|rrze-rsvp | Platzbuchungssystem und Raumverwaltung|[GitHub](https://github.com/RRZE-Webteam/rrze-rsvp) [Docs](https://www.wordpress.rrze.fau.de/plugins/neue-inhaltstypen-erstellen/rsvp/)| 
|rrze-xliff  |Generiert Übersetzungsdateien für das Sprachenzentrum im XLIFF Format|[GitHub](https://github.com/RRZE-Webteam/rrze-xliff)|
|rrze-downloads |Kategoriesierung/Tags in Mediathek, Listen von Dateien|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/downloads.rrze.fau.de) [Docs](https://www.wordpress.rrze.fau.de/plugins/inhaltsseiten-mit-funktionen-erweitern/rrze-downloads/)| 
|fau-oembed  |Einbindung von oEmbed-Daten (FAU Kartendienst, Slideshare und Co.)|[GitHub](https://github.com/RRZE-Webteam/fau-oembed) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/fau-oembed/)|

## Netzwerk-Plugins, sowie RRZE-interne Verwaltungsplugins

|Plugin-Name|Kurzbeschreibung|Links|ToDos oder Info|Ansprechpartner
|---|---|---|---|---|
|rrze-sso   |SSO-Anmeldungssystem|[GitHub](https://github.com/RRZE-Webteam/rrze-sso)|Aktuell|Rolf
|rrze-settings   |CMS-Settings|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-settings)|Aktuell|Rolf
|rrze-rsvp-network   |Erweiterung für RSVP: Daten-Vorhaltung|[GitHub](https://github.com/RRZE-Webteam/rrze-rsvp-network)|
|rrze-cli   |WP-CLI Erweiterung für die CMS-Verwaltung des RRZE.|[GitHub](https://github.com/RRZE-Webteam/rrze-cli)||Rolf
|rrze-cmsinfo   | Ausgabe der Theme und Plugins-Infos zum CMS Dienst für die RRZE-Website|[GitHub](https://github.com/RRZE-Webteam/rrze-cmsinfo)|Aktuell|Rolf
|rrze-updater   |Updatet Plugins aus den GitHub / GitLab-Verzeichnissen|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-updater)|Aktuell|Rolf
|rrze-wp-cron  |Verwaltung des WP_Cron.|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-wp-cron)|Aktuell|Rolf
|rrze-log   |Log-System für das CMS (Registriert Fehlermeldungen im Backend)|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-log)|Aktuell|Rolf
|rrze-netzwerk-audit ||[GitHub](https://gitlab.rrze.fau.de/rrze-webteam/rrze-netzwerk-audit)|Aktuell|Rolf
|rrze-cache  |CMS-Cache|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-cache)|Aktuell| Rolf
|rrze-notices |Netzwerkweite Hinweise und Wartungsmeldungen. Unterstützung für wiederkehrende Termine.|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-notices)|Aktuell|Rolf


## Sepzial-Plugins für Themes oder für einzelne Websites
|Plugin-Name|Kurzbeschreibung|Links|ToDos oder Info|Ansprechpartner
|---|---|---|---|---|
|fau-jobportal   |Ausgabe von Daten aus FAU Jobs.|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/fau-jobportal) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/jobs/)| für jobs.fau.de | Barbara
|rrze-jobalert| JobAlert-Funktion für das Jobportal|für jobs.fau.de|Barbara 
|fau-orga-breadcrumb |Ausgabe der Brotkrümennavigation im FAU Theme|[GitHub](https://github.com/RRZE-Webteam/fau-orga-breadcrumb)| 
|rrze-siteimprove   |Support für Siteimprove|[GitHub](https://github.com/RRZE-Webteam/rrze-siteimprove)|Aktuell|Rolf
|rrze-kurse |  Ausgabe der Schulungszentrumskurse für die RRZE Seiten |[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-kurse)||Barbara
|rrze-preise | Ausgabe von Preisen auf den RRZE-Seiten|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-preise)||Barbara
| Short-URL-Service|Neubau des aktuellen Short-URL Service bei Beibehaltung der bisherigen ShortURLs und der Berechnung dieser. Allerdings Umbau als Plugin mit Berechtigungen für FAU Angehörige und Nutzung auch für andere FAU-URLs. Keine lesenden Anbindungen, aber Kenntnis über Aufbau von URLs von div. Plattformen zur Berechnung von URLs zu diesen | [https://github.com/RRZE-Webteam/rrze-shorturl](https://github.com/RRZE-Webteam/rrze-shorturl) || Benjamin



## Plugins, die in Zukunft wegfallen (sollen)

|Plugin-Name|Kurzbeschreibung|Links|ToDo|
|---|---|---|---|
|rrze-ratebutton|Ausgabe eines Like-Buttons auf Inhaltsseiten|[GitHub](https://github.com/RRZE-Webteam/rrze-ratebutton)|Wird noch von 14 Sites verwendet. Ist archiviert.
|rrze-univis   |UnivIS-Ausgabe von Lehrveranstaltungen|[GitHub](https://github.com/RRZE-Webteam/rrze-univis) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/rrze-univis/)|  Weiterentwicklung wahrscheinlich nicht notwendig, da langfristig durch andere Plugins ersetzt; Bis dahin nur am Leben halten...
|rrze-greetings|  Erstellung und Versand von RRZE Grußkarten|[GitHub](https://github.com/RRZE-Webteam/rrze-greetings)| Wird noch von 2 Sites verwendet. Ist archiviert.
|rrze-highcharts|Ausgabe von Highcharts-Diagrammen|[GitLab](https://gitlab.rrze.fau.de/rrze-webteam/rrze-highcharts)| Wird noch von 2 Sites verwendet. Ersetzen durch neues allgemeines Visualisierungs-Plugin. <br>Siehe: [Fehlende Plugins](Fehlende-Plugins.md)  
|rrze-remoter |Ausgabe eines Webspace-Verzeichnisses|[GitHub](https://github.com/RRZE-Webteam/rrze-remoter) [Docs](https://www.wordpress.rrze.fau.de/plugins/externe-daten-einbinden/remoter/)| Ersetzen durch neues Plugin: Refactoring dringend notwendig, ggf. Anbindung an FAUBox und MOVE-Projekt? <br>Siehe: [Fehlende Plugins](Fehlende-Plugins.md)  
|fau-person|Ausgabe von Kontakten und Kontaktübersicht. Integration von UnivIS|[GitHub](https://github.com/RRZE-Webteam/fau-person)|Wird nur noch am Leben gehalten, da FAUdir ersetzen soll|




